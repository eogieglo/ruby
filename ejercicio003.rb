# 3 - Estructura de programación secuencial

puts " "
puts "CALCULANDO SUMA Y PRODUCTO"
puts "Ingrese primer numero"
num1 = gets.to_i
puts "Ingrese segundo numero"
num2 = gets.to_i
suma = num1 + num2
producto = num1 * num2
puts "La suma de los dos valores es "
puts suma
puts "El producto de los dos valores es "
puts producto

# Ejercicios propuestos
=begin
    Realizar la carga del lado de un cuadrado, 
    mostrar por pantalla el perímetro del mismo 
    (El perímetro de un cuadrado se calcula multiplicando 
        el valor del lado por cuatro)
=end

puts " "
puts "CALCULANDO EL LADO DE UN CUADRADO"
puts "Ingrese el lado de un cuadrado "
lado_cuadrado = gets.to_i
perímetro = lado_cuadrado * 4
puts "El perimetro es "
puts perímetro

=begin
    Escribir un programa en el cual se ingresen cuatro números, calcular e informar la suma 
    de los dos primeros y el producto del tercero y el cuarto.
=end

puts " "
puts "CALCULANDO SUMA Y PRODUCTO"
puts " "
puts "Ingrese numero 1 "
num1 = gets.to_i
puts "Ingrese numero 2 "
num2 = gets.to_i
puts "Ingrese numero 3 "
num3 = gets.to_i
puts "Ingrese numero 4 "
num4 = gets.to_i
puts " "
suma_de_1_2 = num1 + num2
producto_de_3_4 = num3 * num4
puts "La suma es "
puts suma_de_1_2
puts "El producto es "
puts producto_de_3_4

=begin
    Realizar un programa que lea 
    cuatro valores numéricos e informar su suma y promedio.
=end
puts " "
puts "CALCULANDO SUMA y PROMEDIO 4 numeros"
puts "Ingrese numero 1 "
num1 = gets.to_i
puts "Ingrese numero 2 "
num2 = gets.to_i
puts "Ingrese numero 3 "
num3 = gets.to_i
puts "Ingrese numero 4 "
num4 = gets.to_i
puts " "
suma = num1 + num2 + num3 + num4
promedio = num1 * num2 * num3 * num4
puts "La suma es "
puts suma
puts "El promedio es "
puts promedio

=begin
    Se debe desarrollar un programa que pida el ingreso del 
    precio de un artículo y la cantidad que lleva el cliente. 
    Mostrar lo que debe abonar el comprador.
    Permitir cargar un valor float para el precio del 
    artículo.
=end

puts " "
puts "CALCULO PRECIO ARTICULO"
puts "Ingrese el precio de 1 articulo"
precio = gets.to_f
puts "Ingrese la cantidad que compro "
cantidad = gets.to_i
precio_total = precio * cantidad
puts "El total de la compra es "
puts precio_total