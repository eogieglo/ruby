# 4 - Estructuras condicionales simples y compuestas

# Ingresar el sueldo de una persona, si supera los 3000 mostrar un mensaje en pantalla 
# indicando que debe abonar impuestos.

puts "Ingrese el sueldo "
sueldo = gets.to_f
if sueldo > 3000
    puts "Esta persona dene abonar impuestos"
end


=begin    
Problema
Realizar un programa que solicite al operador ingresar dos 
números y muestre por pantalla el mayor de ellos.
=end

puts ""
puts "CALCULANDO mayor de 2 numeros"
puts "Ingrese numero 1"
num1 = gets.to_i
puts "Ingrese numero 2"
num2 = gets.to_i
if num1 > num2
    puts " "
    puts "El numero mayor es el 1 con valor ", num1
else
    puts " "
    puts "El numero mayor es el 2 con valor ", num2
end

=begin  
    Problemas propuestos
    A - Realizar un programa que solicite la carga por teclado de dos números, si el primero es mayor al segundo informar su suma 
    y diferencia, en caso contrario informar el producto y la división del primero respecto al segundo.
    B - Se ingresan tres notas de un alumno, si el promedio es mayor o igual a siete mostrar un mensaje "Promocionado".
    C - Se ingresa por teclado un número positivo de uno o dos dígitos (1..99) mostrar un mensaje indicando si el número tiene uno o 
    dos dígitos.(Tener en cuenta que condición debe cumplirse para tener dos dígitos un número entero)
=end

# A #
puts " "
puts "Calculando suma y diferencia de 2 numeros"
puts " "
puts "Ingrese numero 1: \n"
num1 = gets.to_i
puts "Ingrese numero 2: \n"
num2 = gets.to_i
if num1 > num2
    suma = num1 + num2
    diferencia = num1 - num2
    puts " "
    puts "La suma es: \n"
    puts suma
    puts "La diferencia es: \n"
    puts diferencia
    puts " "
else 
    producto = num1 * num2
    division = num1 / num2
    puts " "
    puts "El producto es: \n"
    puts producto
    puts "La division es: \n"
    puts division
    puts " "
end

# B #
puts "NOTA ALUMNO"
puts " "
puts "Ingrese Nota 1: \n"
nota1 = gets.to_f
puts "Ingrese Nota 2: \n"
nota2 = gets.to_f
puts "Ingrese Nota 3: \n"
nota3 = gets.to_f
promedio = (nota1 + nota2 + nota3) / 3
if promedio >= 7
    puts " "
    puts " PROMOCIONADO \n"
end

# C #
puts ""
puts "Calculando cantidad de digitos"
puts "Ingrese un valor entero de 1 o 2 digitos"
num = gets.to_i
if num < 10
  puts "Tiene un digito"
else
  puts "Tiene dos digitos"
end